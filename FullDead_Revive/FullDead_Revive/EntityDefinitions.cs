﻿using FullDead_Revive.EntityComponents;
using Microsoft.Xna.Framework;
using ViruEngine.Entities;
using ViruEngine.Entities.Components;
using ViruEngine.Physics;

namespace FullDead_Revive
{
    public class EntityDefinitions
    {
        public static EntityManager.GameEntityCreationDelegate Player = (game, level) =>
        {
            var gameEntity = new GameEntity(game, level);
            gameEntity.AddComponent<HPComponent>();
            gameEntity.AddComponent<VelocityComponent>();

            gameEntity.AddComponent<CollisionComponent>();
            gameEntity.GetComponent<CollisionComponent>().CollisionBox =
                Polygon.FromRectangle(new Rectangle(0, 0, 32, 32));

            gameEntity.GetComponent<VelocityComponent>().Friction = new Vector2(1, 1);
            gameEntity.GetComponent<VelocityComponent>().TerminalVelocity = new Vector2(5, 5);

            gameEntity.AddComponent<DescriptionComponent>();

            gameEntity.AddComponent<PlayerComponent>();

            gameEntity.UpdateOrder = 1;

            gameEntity.ID = "Player";

            return gameEntity;
        };

        public static EntityManager.GameEntityCreationDelegate Infected = (game, level) =>
        {
            var gameEntity = new GameEntity(game, level);
            gameEntity.AddComponent<HPComponent>();
            gameEntity.AddComponent<VelocityComponent>();

            gameEntity.AddComponent<CollisionComponent>();
            gameEntity.GetComponent<CollisionComponent>().CollisionBox =
                Polygon.FromRectangle(new Rectangle(0, 0, 32, 32));

            gameEntity.GetComponent<VelocityComponent>().Friction = new Vector2(0.5f, 0.5f);
            gameEntity.GetComponent<VelocityComponent>().TerminalVelocity = new Vector2(3, 3);


            gameEntity.AddComponent<DescriptionComponent>();

            gameEntity.AddComponent<InfectedComponent>();

            gameEntity.UpdateOrder = 1;

            gameEntity.ID = "Infected";

            return gameEntity;
        };
    }
}