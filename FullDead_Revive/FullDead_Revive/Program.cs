namespace FullDead_Revive
{
    internal static class Program
    {
        public static bool Dev;

        /// <summary>
        ///     Vstupn� metoda programu.
        /// </summary>
        private static void Main(string[] args)
        {
            if (args.Length != 0) Dev = true;

            using (var game = new FullDead())
            {
                game.Run();
            }
        }
    }
}