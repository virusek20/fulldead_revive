﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ViruEngine;
using ViruEngine.GameStates;
using ViruEngine.GUI;

namespace FullDead_Revive.GameStates
{
    public class SelectModeState : GameState
    {
        private Texture2D _bgrPlay;
        private ViruGame _game;
        private Picker _gameTypePicker;

        /// <summary>
        ///     Inicializuje instanci třídy MenuState.
        ///     Není potřeba volat, hra si tuto metodu volá sama.
        /// </summary>
        /// <param name="game">Reference na hlavní třídu. Potřeba k volání určitých metod</param>
        public override void Initialize(ViruGame game)
        {
            _game = game;

            new Button(_game, this, "backButton")
            {
                Position = new Vector2(8, _game.Graphics.PreferredBackBufferHeight - 32),
                OnResize = () => new Vector2(8, _game.Graphics.PreferredBackBufferHeight - 32),
                OnClick =
                    () =>
                        _game.TransitionState.DoTransition(_bgrPlay, "Backgrounds/menu", (_game as FullDead).MenuState),
                Text = "Back",
                Priority = 0,
                Visibility = HiddenState.Visible
            };

            new Button(_game, this, "playButton")
            {
                Position = new Vector2(8, _game.Graphics.PreferredBackBufferHeight - 64),
                OnResize = () => new Vector2(8, _game.Graphics.PreferredBackBufferHeight - 64),
                Text = "Play",
                Priority = 0,
                Visibility = HiddenState.Visible,
                OnClick = () =>
                {
                    if (_gameTypePicker.GetSelected() == "Editor")
                    {
                        _game.ActiveState = (_game as FullDead).EditorState;
                    }
                    else if (_gameTypePicker.GetSelected() == "Campaign")
                    {
                        _game.ActiveState = (_game as FullDead).PlayState;
                    }
                }
            };

            _gameTypePicker = new Picker(_game, this, "gameTypePicker")
            {
                Text = new[] {"Editor", "Campaign", "Random"},
                Positon = new Vector2(25, 25),
                OnResize = () => new Vector2(25, 25),
                TextColor = Color.White
            };

            Loaded = true;
        }

        /// <summary>
        ///     Metoda načítající textury.
        ///     Není potřeba volat, hra si tuto metodu volá sama.
        /// </summary>
        /// <param name="content">ContentManager zprostředkovávající načítaní dat pomocí XNA</param>
        public override void LoadContent(ContentManager content)
        {
            _bgrPlay = content.Load<Texture2D>("Backgrounds/play");
        }

        /// <summary>
        ///     Hlavní aktualizační metoda tohoto stavu.
        ///     Není potřeba volat, hra si tuto metodu volá sama.
        /// </summary>
        public override void Update(GameTime gameTime)
        {
            var mState = Mouse.GetState();
            var mPoint = new Point(mState.X, mState.Y);

            DoCheck(gameTime, mPoint, _game.JustPressed());
        }

        /// <summary>
        ///     Hlavní vykreslovací metoda tohoto stavu.
        ///     Není potřeba volat, hra si tuto metodu volá sama.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            _game.SpriteBatch.Begin();

            _game.SpriteBatch.Draw(_bgrPlay, new Vector2(0, 0), null, Color.White, 0, new Vector2(0, 0),
                new Vector2(_game.Graphics.PreferredBackBufferWidth/1920f,
                    _game.Graphics.PreferredBackBufferHeight/1080f), SpriteEffects.None, 0);

            DoDraw(gameTime, _game.SpriteBatch);

            _game.SpriteBatch.End();
        }

        /// <summary>
        ///     Metoda znovu vypočítá pozici všech tlačítek.
        ///     Užitečné například při změně rozlišení.
        /// </summary>
        public override void RecalculateSizes()
        {
            DoResize();
        }
    }
}