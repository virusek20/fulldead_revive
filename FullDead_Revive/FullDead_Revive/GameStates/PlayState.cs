﻿using FullDead_Revive.EntityComponents;
using FullDead_Revive.WorldComponents;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ViruEngine;
using ViruEngine.GameStates;
using ViruEngine.Graphics.Textures;
using ViruEngine.World;

namespace FullDead_Revive.GameStates
{
    public class PlayState : GameState
    {
        public GameLevel GameWorld;

        public override void Initialize(ViruGame game)
        {
            base.Initialize(game);

            GameWorld = new GameLevel(game, 100, 100);
            GameWorld.AddComponent<WorldCollisionComponent>();
            GameWorld.AddComponent<WorldTileManagerComponent>().AllocateTiles(100, 100);
            GameWorld.AddComponent<WorldNPCManagerComponent>();

            GameWorld.Background = new AnimatedTexture(Game.Content.Load<Texture2D>("border"),
                new Vector2(Game.Graphics.PreferredBackBufferWidth, Game.Graphics.PreferredBackBufferHeight))
            {
                Origin = Vector2.Zero
            };

            GameWorld.PlayerEntity = GameWorld.GetComponent<WorldNPCManagerComponent>()
                .CreateNPC(null, "Player", Vector2.Zero);

            GameWorld.GetComponent<WorldNPCManagerComponent>()
                .CreateNPC(null, "Infected", new Vector2(1000, 1000));


            for (var i = 0; i < 100; i++)
            {
                for (var j = 0; j < 100; j++)
                {
                    GameWorld.GetComponent<WorldTileManagerComponent>().CreateTile(null, "grass", new Vector2(i, j));
                }
            }

            GameWorld.GetComponent<WorldTileManagerComponent>().CreateTile(null, "house", new Vector2(6, 6));
            GameWorld.GetComponent<WorldTileManagerComponent>().CreateTile(null, "house", new Vector2(4, 4));
            GameWorld.GetComponent<WorldTileManagerComponent>().CreateTile(null, "house", new Vector2(4, 6));
            GameWorld.GetComponent<WorldTileManagerComponent>().CreateTile(null, "house", new Vector2(6, 4));

            for (var i = -3; i < 4; i++)
            {
                GameWorld.GetComponent<WorldTileManagerComponent>().CreateTile(null, "road", new Vector2(5, 5 + i));

                if (i == 0) continue;

                GameWorld.GetComponent<WorldTileManagerComponent>().CreateTile(null, "road", new Vector2(5 + i, 5));
            }

            GameWorld.GetComponent<WorldTileManagerComponent>().CreateTile(null, "road", new Vector2(3, 6));
            GameWorld.GetComponent<WorldTileManagerComponent>().CreateTile(null, "road", new Vector2(6, 3));

            GameWorld.GetComponent<WorldTileManagerComponent>().CreateTile(null, "road", new Vector2(4, 7));
            GameWorld.GetComponent<WorldTileManagerComponent>().CreateTile(null, "road", new Vector2(7, 4));

            GameWorld.GetComponent<WorldTileManagerComponent>().CreateTile(null, "mineral", new Vector2(3, 3));
            GameWorld.GetComponent<WorldTileManagerComponent>().CreateTile(null, "mineral", new Vector2(7, 3));
            GameWorld.GetComponent<WorldTileManagerComponent>().CreateTile(null, "mineral", new Vector2(3, 7));
            GameWorld.GetComponent<WorldTileManagerComponent>().CreateTile(null, "mineral", new Vector2(7, 7));

            GameWorld.Init(game, null);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            GameWorld.Background.Shift = GameWorld.PlayerEntity.Position;

            GameWorld.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            Game.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.AnisotropicWrap,
                DepthStencilState.Default, RasterizerState.CullNone);

            GameWorld.DrawBackground(gameTime);

            Game.SpriteBatch.End();

            Game.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.AnisotropicWrap,
                DepthStencilState.Default, RasterizerState.CullNone, null,
                GameWorld.PlayerEntity.GetComponent<PlayerComponent>().Camera.GetTransformation(Game.GraphicsDevice));

            GameWorld.Draw(gameTime);

            Game.SpriteBatch.End();

            Game.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.AnisotropicWrap,
                DepthStencilState.Default, RasterizerState.CullNone);

            GameWorld.PlayerEntity.GetComponent<PlayerComponent>().DrawGUI(gameTime, Game.SpriteBatch);
            if (!GameWorld.PlayerEntity.GetSpawnedState())
                Game.SpriteBatch.DrawString(Game.BasicFont, "YOU DIED",
                    new Vector2(
                        Game.Graphics.PreferredBackBufferWidth/2 - Game.BasicFont.MeasureString("YOU DIED").X*5/2,
                        Game.Graphics.PreferredBackBufferHeight/2 - Game.BasicFont.MeasureString("YOU DIED").Y*5/2),
                    Color.Red, 0f, Vector2.Zero, new Vector2(5), SpriteEffects.None, 0);

            Game.SpriteBatch.End();
        }
    }
}