﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ViruEngine;
using ViruEngine.GameStates;
using ViruEngine.GUI;

namespace FullDead_Revive.GameStates
{
    public class MenuState : GameState
    {
        private Texture2D _bgrMenu;
        private YesNoDialog _exitDialog;
        private ViruGame _game;
        private Texture2D _logo;

        /// <summary>
        ///     Inicializuje instanci třídy MenuState.
        ///     Není potřeba volat, hra si tuto metodu volá sama.
        /// </summary>
        /// <param name="game">Reference na hlavní třídu. Potřeba k volání určitých metod</param>
        public override void Initialize(ViruGame game)
        {
            _game = game;

            _exitDialog = new YesNoDialog(_game, this, "exitDialog")
            {
                OnYes = _game.Exit,
                Position =
                    new Vector2(_game.Graphics.PreferredBackBufferWidth/2 - 300,
                        _game.Graphics.PreferredBackBufferHeight/2 - 75),
                OnResize =
                    () =>
                        new Vector2(_game.Graphics.PreferredBackBufferWidth/2 - 300,
                            _game.Graphics.PreferredBackBufferHeight/2 - 75),
                OnNo = () => _exitDialog.Hide(),
                Text = "Are you sure you want to\nexit the game?",
                Priority = 2
            };

            new Button(_game, this, "exitButton")
            {
                Position = new Vector2(8, _game.Graphics.PreferredBackBufferHeight - 32),
                Text = "Exit",
                OnClick = () => _exitDialog.Show(),
                OnResize = () => new Vector2(8, _game.Graphics.PreferredBackBufferHeight - 32)
            };

            new Button(_game, this, "optionsButton")
            {
                Position = new Vector2(8, _game.Graphics.PreferredBackBufferHeight - 64),
                Text = "Options",
                OnClick =
                    () =>
                        _game.TransitionState.DoTransition(_bgrMenu, "Backgrounds/options",
                            (_game as FullDead).OptionsState),
                OnResize = () => new Vector2(8, _game.Graphics.PreferredBackBufferHeight - 64)
            };

            new Button(_game, this, "playButton")
            {
                Position = new Vector2(8, _game.Graphics.PreferredBackBufferHeight - 96),
                Text = "Play",
                OnClick =
                    () =>
                        _game.TransitionState.DoTransition(_bgrMenu, "Backgrounds/play",
                            (_game as FullDead).SelectModeState),
                OnResize = () => new Vector2(8, _game.Graphics.PreferredBackBufferHeight - 96)
            };

            Loaded = true;
        }

        /// <summary>
        ///     Metoda načítající textury.
        ///     Není potřeba volat, hra si tuto metodu volá sama.
        /// </summary>
        /// <param name="content">ContentManager zprostředkovávající načítaní dat pomocí XNA</param>
        public override void LoadContent(ContentManager content)
        {
            _bgrMenu = content.Load<Texture2D>("Backgrounds/menu");
            _logo = content.Load<Texture2D>("Backgrounds/logo");
        }

        /// <summary>
        ///     Hlavní aktualizační metoda tohoto stavu.
        ///     Není potřeba volat, hra si tuto metodu volá sama.
        /// </summary>
        public override void Update(GameTime gameTime)
        {
            var mState = Mouse.GetState();
            var mPoint = new Point(mState.X, mState.Y);

            DoCheck(gameTime, mPoint, _game.JustPressed());
        }

        /// <summary>
        ///     Hlavní vykreslovací metoda tohoto stavu.
        ///     Není potřeba volat, hra si tuto metodu volá sama.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            _game.SpriteBatch.Begin();

            _game.SpriteBatch.Draw(_bgrMenu, new Vector2(0, 0), null, Color.White, 0, new Vector2(0, 0),
                new Vector2(_game.Graphics.PreferredBackBufferWidth/1920f,
                    _game.Graphics.PreferredBackBufferHeight/1080f), SpriteEffects.None, 0);
            _game.SpriteBatch.Draw(_logo,
                new Vector2(
                    _game.Graphics.PreferredBackBufferWidth -
                    _game.Graphics.PreferredBackBufferWidth*0.125f, 0), null, Color.White, 0,
                new Vector2(0, 0),
                new Vector2(_game.Graphics.PreferredBackBufferWidth/220f*0.125f,
                    _game.Graphics.PreferredBackBufferWidth/220f*0.125f), SpriteEffects.None,
                0);

            DoDraw(gameTime, _game.SpriteBatch);

            _game.SpriteBatch.End();
        }

        /// <summary>
        ///     Metoda znovu vypočítá pozici všech tlačítek.
        ///     Užitečné například při změně rozlišení.
        /// </summary>
        public override void RecalculateSizes()
        {
            DoResize();
        }
    }
}