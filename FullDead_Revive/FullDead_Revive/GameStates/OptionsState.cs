﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ViruEngine;
using ViruEngine.GameStates;
using ViruEngine.GUI;
using ViruEngine.PersistentData;

namespace FullDead_Revive.GameStates
{
    public class OptionsState : GameState
    {
        private static readonly string[] Resolutions =
        {
            "960x540", "1024x576", "1280x720", "1366x768",
            "1600x900", "1920x1080"
        };

        private Button _applyButton;
        private Texture2D _bgrOptions;
        private CheckBox _fullscreenCheckBox;
        private ViruGame _game;
        private Picker _resolutionPicker;
        private Label _volumeLabel;
        private Slider _volumeSlider;
        public Color ConsoleColor = new Color(70, 70, 70);

        /// <summary>
        ///     Inicializuje instanci třídy OptionsState.
        ///     Není potřeba volat, hra si tuto metodu volá sama.
        /// </summary>
        /// <param name="game">Reference na hlavní třídu. Potřeba k volání určitých metod</param>
        public override void Initialize(ViruGame game)
        {
            _game = game;

            _volumeSlider = new Slider(_game, this, "volumeSlider")
            {
                Position = new Vector2(25, 25),
                OnResize = () => new Vector2(25, 25),
                Progress = Settings.Volume,
                Width = 200
            };

            _fullscreenCheckBox = new CheckBox(_game, this, "fullscreenBox")
            {
                Text = "Fullscreen",
                Position = new Vector2(25, 85),
                OnResize = () => new Vector2(25, 85),
                Toggled = Settings.Fullscreen,
                CheckBoxCheckColor = Color.Lime
            };

            _resolutionPicker = new Picker(_game, this, "resolutionPicker")
            {
                Positon = new Vector2(25, 60),
                OnResize = () => new Vector2(25, 60),
                Text = Resolutions,
                TextColor = Color.White
            };

            _volumeLabel = new Label(_game, this, "volumeLabel")
            {
                Text = "Volume: " + Math.Floor(_volumeSlider.Progress*100) + "%",
                Position = new Vector2(25, 5),
                OnResize = () => new Vector2(25, 5)
            };

            new Label(_game, this, "resolutionLabel")
            {
                Text = "Resolution: ",
                Position = new Vector2(25, 40),
                OnResize = () => new Vector2(25, 40)
            };

            new Label(_game, this, "fullscreenInfoLabel")
            {
                Text = "(Uses your desktop resolution)",
                Position = new Vector2(185, 87),
                OnResize = () => new Vector2(185, 87)
            };

            for (var i = 0; i < Resolutions.Length; i++)
            {
                if (Settings.Resolution.Width == int.Parse(Resolutions[i].Split('x')[0]))
                    _resolutionPicker.Selection = i;
            }

            new Button(_game, this, "backButton")
            {
                Position = new Vector2(8, _game.Graphics.PreferredBackBufferHeight - 32),
                OnResize = () => new Vector2(8, _game.Graphics.PreferredBackBufferHeight - 32),
                Text = "Back",
                OnClick =
                    () =>
                        _game.TransitionState.DoTransition(_bgrOptions, "Backgrounds/menu",
                            (_game as FullDead).MenuState),
                Priority = 0,
                Visibility = HiddenState.Visible
            };

            new Button(_game, this, "acceptButton")
            {
                Position = new Vector2(8, _game.Graphics.PreferredBackBufferHeight - 64),
                OnResize = () => new Vector2(8, _game.Graphics.PreferredBackBufferHeight - 64),
                Text = "Accept",
                OnClick = () =>
                {
                    _applyButton.OnClick();
                    _game.TransitionState.DoTransition(_bgrOptions, "Backgrounds/menu", (_game as FullDead).MenuState);
                },
                Priority = 0,
                Visibility = HiddenState.Visible
            };

            _applyButton = new Button(_game, this, "applyButton")
            {
                Position = new Vector2(8, _game.Graphics.PreferredBackBufferHeight - 96),
                OnResize = () => new Vector2(8, _game.Graphics.PreferredBackBufferHeight - 96),
                Text = "Apply",
                Priority = 0,
                Visibility = HiddenState.Visible
            };

            _applyButton.OnClick = () =>
            {
                Settings.Volume = _volumeSlider.Progress;
                Settings.Resolution = new Resolution
                {
                    Width = int.Parse(_resolutionPicker.GetSelected().Split('x')[0]),
                    Height = int.Parse(_resolutionPicker.GetSelected().Split('x')[1])
                };

                Settings.Fullscreen = _fullscreenCheckBox.Toggled;

                (_game as FullDead).ApplyGraphics();

                _game.LoadFont();

                Settings.Save();
            };

            Loaded = true;
        }

        /// <summary>
        ///     Metoda načítající textury.
        ///     Není potřeba volat, hra si tuto metodu volá sama.
        /// </summary>
        /// <param name="content">ContentManager zprostředkovávající načítaní dat pomocí XNA</param>
        public override void LoadContent(ContentManager content)
        {
            _bgrOptions = content.Load<Texture2D>("Backgrounds/options");
        }

        /// <summary>
        ///     Hlavní aktualizační metoda tohoto stavu.
        ///     Není potřeba volat, hra si tuto metodu volá sama.
        /// </summary>
        public override void Update(GameTime gameTime)
        {
            var mState = Mouse.GetState();
            var mPoint = new Point(mState.X, mState.Y);

            DoCheck(gameTime, mPoint, _game.JustPressed());

            _volumeLabel.Text = "Volume: " + Math.Floor(_volumeSlider.Progress*100) + "%";
        }

        /// <summary>
        ///     Hlavní vykreslovací metoda tohoto stavu.
        ///     Není potřeba volat, hra si tuto metodu volá sama.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            _game.SpriteBatch.Begin();

            _game.SpriteBatch.Draw(_bgrOptions, new Vector2(0, 0), null, Color.White, 0, new Vector2(0, 0),
                new Vector2(_game.Graphics.PreferredBackBufferWidth/1920f,
                    _game.Graphics.PreferredBackBufferHeight/1080f), SpriteEffects.None, 0);

            DoDraw(gameTime, _game.SpriteBatch);

            _game.SpriteBatch.End();
        }

        /// <summary>
        ///     Metoda znovu vypočítá pozici všech tlačítek.
        ///     Užitečné například při změně rozlišení.
        /// </summary>
        public override void RecalculateSizes()
        {
            DoResize();
        }
    }
}