using FullDead_Revive.GameStates;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ViruEngine;
using ViruEngine.Console;
using ViruEngine.Entities.Components;
using ViruEngine.PersistentData;

namespace FullDead_Revive
{
    public class FullDead : ViruGame
    {
        // V�echny stavy. Kdy� m�n� stavy pou��vej ji� tyto definovan�.
        public EditorState EditorState;
        public MenuState MenuState;
        public OptionsState OptionsState;
        public PlayState PlayState;
        public SelectModeState SelectModeState;

        /// <summary>
        ///     Inicializuje novou instanci hry.
        ///     Nen� pot�eba volat, XNA si tuto metodu vol� s�m.
        /// </summary>
        public FullDead() : base("FullDead")
        {
        }

        /// <summary>
        ///     Nastav� rozli�en� a fullscreen podle nastaven�.
        /// </summary>
        /// <param name="start">Ur�uje zdali se hra pr�v� zap�n�.</param>
        public void ApplyGraphics(bool start = false)
        {
            if (Settings.Fullscreen == Graphics.IsFullScreen)
            {
                if (Settings.Fullscreen)
                {
                    var displayMode = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode;
                    Graphics.PreferredBackBufferWidth = displayMode.Width;
                    Graphics.PreferredBackBufferHeight = displayMode.Height;
                }
                else
                {
                    Graphics.PreferredBackBufferWidth = Settings.Resolution.Width;
                    Graphics.PreferredBackBufferHeight = Settings.Resolution.Height;
                }

                Graphics.ApplyChanges();
                MainRenderTarget = new RenderTarget2D(GraphicsDevice, Graphics.PreferredBackBufferWidth,
                    Graphics.PreferredBackBufferHeight);

                if (start) return;

                GameConsole.RecalculateSizes();

                if (MenuState.Loaded) MenuState.RecalculateSizes();
                if (OptionsState.Loaded) OptionsState.RecalculateSizes();
                if (PlayState.Loaded) PlayState.RecalculateSizes();
                if (TransitionState.Loaded) TransitionState.RecalculateSizes();
                if (EditorState.Loaded) EditorState.RecalculateSizes();
                if (SelectModeState.Loaded) SelectModeState.RecalculateSizes();

                return;
            }

            if (Settings.Fullscreen)
            {
                var displayMode = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode;
                Graphics.PreferredBackBufferWidth = displayMode.Width;
                Graphics.PreferredBackBufferHeight = displayMode.Height;
                Graphics.ApplyChanges();
                Graphics.ToggleFullScreen();
            }
            else
            {
                if (Graphics.IsFullScreen) Graphics.ToggleFullScreen();
                Graphics.PreferredBackBufferWidth = Settings.Resolution.Width;
                Graphics.PreferredBackBufferHeight = Settings.Resolution.Height;
            }

            Graphics.ApplyChanges();
            MainRenderTarget = new RenderTarget2D(GraphicsDevice, Graphics.PreferredBackBufferWidth,
                Graphics.PreferredBackBufferHeight);

            if (start) return;

            if (MenuState.Loaded) MenuState.RecalculateSizes();
            if (OptionsState.Loaded) OptionsState.RecalculateSizes();
            if (PlayState.Loaded) PlayState.RecalculateSizes();
            if (TransitionState.Loaded) TransitionState.RecalculateSizes();
            if (EditorState.Loaded) EditorState.RecalculateSizes();
            if (SelectModeState.Loaded) SelectModeState.RecalculateSizes();

            GameConsole.RecalculateSizes();
        }

        /// <summary>
        ///     Inicializa�n� metoda FullDead.
        ///     Vytvo�� stavy, inicializuje konzoli a nastav� hlasitot.
        ///     Nen� pot�eba volat, XNA si tuto metodu vol� s�m.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();

            MenuState = new MenuState();
            OptionsState = new OptionsState();
            SelectModeState = new SelectModeState();
            PlayState = new PlayState();
            EditorState = new EditorState();

            ApplyGraphics(true);

            TileComponent.TileSize = new Vector2(128, 128);

            if (!Program.Dev) ActiveState = MenuState;
            else ActiveState = PlayState;
        }

        /// <summary>
        ///     Na�te hern� pole.
        ///     Nen� pot�eba volat, hra si tuto metodu vol� sama.
        /// </summary>
        public void LoadTiles()
        {
            TilesLoader.LoadTiles(Content);
        }

        /// <summary>
        ///     Na�te textury, font a nastav� hern� stav na menu.
        ///     Nen� pot�eba volat, XNA si tuto metodu vol� s�m.
        /// </summary>
        protected override void LoadContent()
        {
            base.LoadContent();

            LoadTiles();
        }
    }
}