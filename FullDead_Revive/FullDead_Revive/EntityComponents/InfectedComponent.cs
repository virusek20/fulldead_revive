﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ViruEngine;
using ViruEngine.ComponentSystem;
using ViruEngine.Entities;
using ViruEngine.Entities.Components;
using ViruEngine.Graphics.Textures;
using ViruEngine.Physics;

namespace FullDead_Revive.EntityComponents
{
    internal class InfectedComponent : Component<GameEntity>
    {
        private double _animationTimer;
        public float Damage = 0.1f;
        public AnimatedTexture InfectedTexture;

        public InfectedComponent(ViruGame game, GameEntity parent) : base(game, parent)
        {
        }

        public override void Init(GameTime gameTime)
        {
            base.Init(gameTime);

            InfectedTexture = new AnimatedTexture(Game.Content.Load<Texture2D>("Characters/NPC/infected_allstates"),
                new Vector2(32, 32));
        }

        public override bool CheckAvailability(GameEntity gameEntity)
        {
            return gameEntity.HasComponent<VelocityComponent>() & gameEntity.HasComponent<CollisionComponent>() &
                   gameEntity.HasComponent<HPComponent>() & gameEntity.HasComponent<DescriptionComponent>();
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);

            if (_animationTimer > 100)
            {
                _animationTimer = 0;
                InfectedTexture.NextFrame();
            }

            InfectedTexture.Draw(Parent.Position);
        }

        public override void Update(GameTime gameTime)
        {
            _animationTimer += gameTime.ElapsedGameTime.TotalMilliseconds;

            Parent.GetComponent<DescriptionComponent>().Description = "HP: " + Parent.GetComponent<HPComponent>().HP +
                                                                      "/" + Parent.GetComponent<HPComponent>().MaxHP +
                                                                      "\n  Level: 1";

            if (Parent.Level.PlayerEntity.GetSpawnedState())
            {
                Parent.GetComponent<VelocityComponent>().Velocity +=
                    Vector2.Normalize(Parent.Level.PlayerEntity.Position - Parent.Position)*
                    Parent.GetComponent<VelocityComponent>().Acceleration;
            }
            else
            {
                Parent.GetComponent<VelocityComponent>().Velocity +=
                    Parent.GetComponent<VelocityComponent>().Acceleration;
            }


            if (Parent.GetComponent<VelocityComponent>().Velocity != Vector2.Zero)
            {
                InfectedTexture.Rotation = (float) Math.Atan2(Parent.GetComponent<VelocityComponent>().Velocity.X,
                    -Parent.GetComponent<VelocityComponent>().Velocity.Y);
            }

            Parent.GetComponent<CollisionComponent>()
                .CollisionBox.Offset(new Vector(Parent.GetComponent<VelocityComponent>().Velocity.X,
                    Parent.GetComponent<VelocityComponent>().Velocity.Y));
        }
    }
}