﻿using System;
using FullDead_Revive.WorldComponents;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ViruEngine;
using ViruEngine.ComponentSystem;
using ViruEngine.Entities;
using ViruEngine.Entities.Components;
using ViruEngine.Graphics.Colors;
using ViruEngine.Graphics.Textures;
using ViruEngine.Physics;
using ViruEngine.World;

namespace FullDead_Revive.EntityComponents
{
    public class WorldTileComponent : Component<GameEntity>
    {
        public delegate void NeighbourUpdateDelegate(ViruGame game, GameEntity tile, GameEntity updated, GameLevel world);
        public delegate string InfoGenerator(ViruGame game, GameEntity tile);

        public delegate void TileActionDelegate(ViruGame game, GameEntity tile, GameLevel world);

        public TileInfo TileInfo;

        public WorldTileComponent(ViruGame game, GameEntity parent) : base(game, parent)
        {
        }

        public override bool CheckAvailability(GameEntity gameEntity)
        {
            return Parent.HasComponent<TileComponent>();
        }

        public override void Init(GameTime gameTime)
        {
            TileInfo.TileTexture.Offset = TileComponent.TileSize;
            if (!TileInfo.Walkable)
                Parent.GetComponent<TileComponent>().CollisionBox.Offset(new Vector(Parent.Position));

            if (TileInfo.OnPlace != null) TileInfo.OnPlace(Game, Parent, Parent.Level);

            Parent.Level.GetComponent<WorldTileManagerComponent>().UpdateNeighbours(Parent);

            if (TileInfo.RandomTexture)
                TileInfo.TileTexture.FrameNumber = Game.Random.Next(TileInfo.TileTexture.Frames);
            if (TileInfo.RandomColor) TileInfo.TileTexture.Tint = ColorRandomizer.GetRandomColor();
            if (TileInfo.RandomRotation)
            {
                TileInfo.TileTexture.Rotation = (float) Math.PI*Game.Random.Next(4);
                if (Parent.HasComponent<CollisionComponent>())
                    Parent.GetComponent<CollisionComponent>().CollisionBox =
                        Parent.GetComponent<CollisionComponent>().CollisionBox.Rotate(TileInfo.TileTexture.Rotation);
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            TileInfo.TileTexture.Draw(Parent.GetComponent<TileComponent>().ResolvePosition());
        }
    }

    public struct TileInfo
    {
        public bool BuildableOver;
        public TileCategory Category;
        public Polygon CollisionBox;
        public string Description;
        public string ID;
        public string Name;
        public WorldTileComponent.TileActionDelegate OnDestroy;
        public WorldTileComponent.NeighbourUpdateDelegate OnNeighbourUpdate;
        public WorldTileComponent.TileActionDelegate OnPlace;
        public WorldTileComponent.TileActionDelegate OnUse;
        public WorldTileComponent.InfoGenerator GetInfo;
        public bool RandomColor;
        public bool RandomRotation;
        public bool RandomTexture;
        public AnimatedTexture TileTexture;
        public bool Walkable;
    }
}