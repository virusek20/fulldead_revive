﻿using ViruEngine;
using ViruEngine.ComponentSystem;
using ViruEngine.Entities;

namespace FullDead_Revive.EntityComponents
{
    class DescriptionComponent : Component<GameEntity>
    {
        public string Description;

        public DescriptionComponent(ViruGame game, GameEntity parent) : base(game, parent)
        {
        }
    }
}
