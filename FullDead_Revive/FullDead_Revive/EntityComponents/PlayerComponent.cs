﻿using System;
using FullDead_Revive.WorldComponents;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ViruEngine;
using ViruEngine.ComponentSystem;
using ViruEngine.Entities;
using ViruEngine.Entities.Components;
using ViruEngine.Graphics.Colors;
using ViruEngine.Graphics.Rendering;
using ViruEngine.Graphics.Textures;
using ViruEngine.GUI;
using ViruEngine.Physics;

namespace FullDead_Revive.EntityComponents
{
    internal class PlayerComponent : Component<GameEntity>
    {
        public Camera2D Camera;
        public int GreenCrystal;
        public ComposedTexture PlayerTexture;

        public PlayerComponent(ViruGame game, GameEntity parent)
            : base(game, parent)
        {
        }

        public override void Init(GameTime gameTime)
        {
            base.Init(gameTime);

            Camera = new Camera2D(Game) { Centered = true };
            PlayerTexture = new ComposedTexture(Game.Content.Load<Texture2D>("Characters/Player/player_allstates"));

            PlayerTexture.IncludedFrames.Add((int)PlayerTextureState.HoldingSMG);
            PlayerTexture.IncludedFrames.Add((int)PlayerTextureState.Body);
            PlayerTexture.IncludedFrames.Add((int)PlayerTextureState.Head);

            Parent.GetComponent<CollisionComponent>().OnCollision += (time, collider, collider2, result) =>
            {
                if (collider.ID == "Infected")
                    Parent.GetComponent<HPComponent>().HP -= collider.GetComponent<InfectedComponent>().Damage;
            };
        }

        public override bool CheckAvailability(GameEntity gameEntity)
        {
            return gameEntity.HasComponent<VelocityComponent>() & gameEntity.HasComponent<CollisionComponent>() &
                   gameEntity.HasComponent<HPComponent>() & gameEntity.HasComponent<DescriptionComponent>();
        }

        public void DrawGUI(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (Game.MState.RightButton == ButtonState.Pressed)
            {
                string tile = "Empty";
                Vector2 mousePos = new Vector2(Game.MState.X, Game.MState.Y) + Parent.Position -
                                   new Vector2(Game.Graphics.PreferredBackBufferWidth/2,
                                       Game.Graphics.PreferredBackBufferHeight/2);

                GameEntity mouseEntity =
                    Parent.Level.GetComponent<WorldNPCManagerComponent>().GetEntityByPosition(mousePos);
                if (mouseEntity != null)
                {
                    tile = mouseEntity.ID;
                    if (mouseEntity.HasComponent<DescriptionComponent>())
                    {
                        tile += "\n  " + mouseEntity.GetComponent<DescriptionComponent>().Description;
                    }
                }
                else
                {
                    GameEntity mouseTileLower =
                        Parent.Level.GetComponent<WorldTileManagerComponent>()
                            .GetTileByPosition(mousePos, 0);

                    if (mouseTileLower != null)
                    {
                        tile = mouseTileLower.GetComponent<WorldTileComponent>().TileInfo.Name + "\n  " +
                               mouseTileLower.GetComponent<WorldTileComponent>().TileInfo.GetInfo(Game, mouseTileLower);
                    }

                    GameEntity mouseTileUpper =
                        Parent.Level.GetComponent<WorldTileManagerComponent>()
                            .GetTileByPosition(mousePos, 1);

                    if (mouseTileUpper != null)
                    {
                        tile = mouseTileUpper.GetComponent<WorldTileComponent>().TileInfo.Name + "\n  " +
                               mouseTileUpper.GetComponent<WorldTileComponent>().TileInfo.GetInfo(Game, mouseTileUpper) +
                               "\n" + tile;

                    }
                }

                MousePopup.Draw(tile.Trim().Replace("\n  \n", "\n"), Color.White,
                    new Vector2(5, -22*tile.Split('\n').Length - 5));
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);

            PlayerTexture.Draw(Parent.Position);
        }

        public override void Update(GameTime gameTime)
        {
            if (Parent.GetComponent<HPComponent>().HP <= 0)
            {
                Parent.SetSpawnedState(gameTime, false);
            }

            Parent.GetComponent<DescriptionComponent>().Description = "HP: " + Math.Round(Parent.GetComponent<HPComponent>().HP) +
                                                                      "/" + Parent.GetComponent<HPComponent>().MaxHP;

            if (Game.KState.IsKeyDown(Keys.W))
                Parent.GetComponent<VelocityComponent>().Velocity.Y +=
                    -Parent.GetComponent<VelocityComponent>().Acceleration.Y;
            if (Game.KState.IsKeyDown(Keys.A))
                Parent.GetComponent<VelocityComponent>().Velocity.X +=
                    -Parent.GetComponent<VelocityComponent>().Acceleration.X;
            if (Game.KState.IsKeyDown(Keys.S))
                Parent.GetComponent<VelocityComponent>().Velocity.Y +=
                    Parent.GetComponent<VelocityComponent>().Acceleration.Y;
            if (Game.KState.IsKeyDown(Keys.D))
                Parent.GetComponent<VelocityComponent>().Velocity.X +=
                    Parent.GetComponent<VelocityComponent>().Acceleration.X;

            if (Parent.GetComponent<VelocityComponent>().Velocity != Vector2.Zero)
            {
                PlayerTexture.Rotation = (float)Math.Atan2(Parent.GetComponent<VelocityComponent>().Velocity.X,
                    -Parent.GetComponent<VelocityComponent>().Velocity.Y);
            }

            Parent.GetComponent<CollisionComponent>()
                .CollisionBox.Offset(new Vector(Parent.GetComponent<VelocityComponent>().Velocity.X,
                    Parent.GetComponent<VelocityComponent>().Velocity.Y));

            Camera.Pos = Parent.Position;
        }

        private enum PlayerTextureState
        {
            HoldingSMG,
            HoldingPistol,
            BodyWeapon,
            Hands,
            Body,
            Head
        }
    }
}