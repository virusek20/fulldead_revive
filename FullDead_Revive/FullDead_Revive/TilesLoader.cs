﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FullDead_Revive.EntityComponents;
using FullDead_Revive.WorldComponents;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ViruEngine;
using ViruEngine.Console;
using ViruEngine.Entities;
using ViruEngine.Entities.Components;
using ViruEngine.Physics;
using ViruEngine.World;

namespace FullDead_Revive
{
    public enum TileCategory
    {
        Buildings,
        Backgrounds,
        Roads,
        PlayerBuilt,
        Natural
    };

    internal class NeighbourPlacement : IEnumerable<bool>
    {
        public bool Down;
        public bool Left;
        public bool Right;
        public bool Up;

        public IEnumerator<bool> GetEnumerator()
        {
            yield return Up;
            yield return Down;
            yield return Left;
            yield return Right;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public static class TilesLoader
    {
        public static void RegisterTile(TileInfo tile)
        {
            EntityManager.Register(tile.ID, (game, level) =>
            {
                var gameEntity = new GameEntity(game, level);

                gameEntity.AddComponent<TileComponent>();
                if (tile.CollisionBox != null)
                    gameEntity.GetComponent<TileComponent>().CollisionBox = tile.CollisionBox.Clone();

                gameEntity.AddComponent<WorldTileComponent>();

                gameEntity.GetComponent<WorldTileComponent>().TileInfo = tile;
                gameEntity.GetComponent<WorldTileComponent>().TileInfo.TileTexture = tile.TileTexture.Clone();

                if (tile.GetInfo == null)
                {
                    gameEntity.GetComponent<WorldTileComponent>().TileInfo.GetInfo = (viruGame, entity) => String.Empty;
                }

                gameEntity.ID = "Tile_" + tile.ID;

                return gameEntity;
            });

            GameConsole.WriteLine("Registered tile " + tile.ID);
        }

        public static void RoadCalculation(ViruGame game, GameEntity tile, GameLevel world)
        {
            var placement = new NeighbourPlacement();

            var neighbour =
                world.GetComponent<WorldTileManagerComponent>()
                    .TryGetEntity(tile.GetComponent<TileComponent>().TilePosition - Vector2.UnitY, 0);
            if (neighbour != null)
            {
                if (neighbour.GetComponent<WorldTileComponent>().TileInfo.ID == "road") placement.Up = true;
            }

            neighbour =
                world.GetComponent<WorldTileManagerComponent>()
                    .TryGetEntity(tile.GetComponent<TileComponent>().TilePosition + Vector2.UnitY, 0);
            if (neighbour != null)
            {
                if (neighbour.GetComponent<WorldTileComponent>().TileInfo.ID == "road") placement.Down = true;
            }

            neighbour =
                world.GetComponent<WorldTileManagerComponent>()
                    .TryGetEntity(tile.GetComponent<TileComponent>().TilePosition - Vector2.UnitX, 0);
            if (neighbour != null)
            {
                if (neighbour.GetComponent<WorldTileComponent>().TileInfo.ID == "road") placement.Left = true;
            }

            neighbour =
                world.GetComponent<WorldTileManagerComponent>()
                    .TryGetEntity(tile.GetComponent<TileComponent>().TilePosition + Vector2.UnitX, 0);
            if (neighbour != null)
            {
                if (neighbour.GetComponent<WorldTileComponent>().TileInfo.ID == "road") placement.Right = true;
            }

            var neighbours = placement.Count(plac => plac);

            switch (neighbours)
            {
                case 0:
                    tile.GetComponent<WorldTileComponent>().TileInfo.TileTexture.FrameNumber = 0;
                    tile.GetComponent<WorldTileComponent>().TileInfo.TileTexture.Rotation = 0f;
                    break;
                case 1:
                    tile.GetComponent<WorldTileComponent>().TileInfo.TileTexture.FrameNumber = 1;

                    if (placement.Up) tile.GetComponent<WorldTileComponent>().TileInfo.TileTexture.Rotation = 0f;
                    else if (placement.Right)
                        tile.GetComponent<WorldTileComponent>().TileInfo.TileTexture.Rotation = (float) Math.PI/2f;
                    else if (placement.Down)
                        tile.GetComponent<WorldTileComponent>().TileInfo.TileTexture.Rotation = (float) Math.PI;
                    else if (placement.Left)
                        tile.GetComponent<WorldTileComponent>().TileInfo.TileTexture.Rotation = 3f/2f*
                                                                                                (float)
                                                                                                    Math.PI;
                    break;
                case 2:
                    tile.GetComponent<WorldTileComponent>().TileInfo.TileTexture.FrameNumber = 2;

                    if (placement.Down & placement.Up)
                        tile.GetComponent<WorldTileComponent>().TileInfo.TileTexture.Rotation = 0f;
                    else if (placement.Left & placement.Right)
                        tile.GetComponent<WorldTileComponent>().TileInfo.TileTexture.Rotation = (float) Math.PI/2f;
                    break;
                case 3:
                    tile.GetComponent<WorldTileComponent>().TileInfo.TileTexture.FrameNumber = 3;

                    if (placement.Left & placement.Right & placement.Up)
                        tile.GetComponent<WorldTileComponent>().TileInfo.TileTexture.Rotation = 0f;
                    else if (placement.Left & placement.Right & placement.Down)
                        tile.GetComponent<WorldTileComponent>().TileInfo.TileTexture.Rotation = (float) Math.PI;
                    else if (placement.Left & placement.Up & placement.Down)
                        tile.GetComponent<WorldTileComponent>().TileInfo.TileTexture.Rotation = 3f/2f*
                                                                                                (float) Math.PI;
                    else if (placement.Right & placement.Up & placement.Down)
                        tile.GetComponent<WorldTileComponent>().TileInfo.TileTexture.Rotation =
                            (float) Math.PI/2f;
                    break;
                case 4:
                    tile.GetComponent<WorldTileComponent>().TileInfo.TileTexture.FrameNumber = 4;
                    tile.GetComponent<WorldTileComponent>().TileInfo.TileTexture.Rotation = 0f;
                    break;
            }
        }

        public static void LoadTiles(ContentManager content)
        {
            RegisterTile(new TileInfo
            {
                BuildableOver = true,
                Description = "Grass.",
                ID = "grass",
                Name = "Grass",
                Walkable = true,
                GetInfo = (game, tile) =>  "Withered",
                TileTexture = content.Load<Texture2D>("Tiles/grass"),
                Category = TileCategory.Backgrounds
            });

            RegisterTile(new TileInfo
            {
                BuildableOver = true,
                Description = "An asphalt road.",
                ID = "road",
                Name = "Road",
                Walkable = true,
                TileTexture = content.Load<Texture2D>("Tiles/roads"),
                RandomTexture = false,
                RandomColor = false,
                Category = TileCategory.Roads,
                OnPlace = (game, tile, world) => { RoadCalculation(game, tile, world); },
                OnNeighbourUpdate = (game, tile, updated, world) => { RoadCalculation(game, tile, world); }
            });

            RegisterTile(new TileInfo
            {
                BuildableOver = false,
                Description = "A simple house.",
                ID = "house",
                Name = "House",
                Walkable = false,
                GetInfo = (game, tile) => "Abandoned",
                RandomColor = false,
                RandomTexture =  true,
                TileTexture = content.Load<Texture2D>("Tiles/Buildings/house"),
                CollisionBox = Polygon.FromRectangle(new Rectangle(6, 10, 105, 90)),
                Category = TileCategory.Buildings
            });

            RegisterTile(new TileInfo
            {
                BuildableOver = false,
                Description = "A resource rich mineral from an alien planet.",
                ID = "mineral",
                Name = "Mineral",
                Walkable = false,
                RandomTexture = false,
                RandomColor = true,
                RandomRotation = true,
                TileTexture = content.Load<Texture2D>("Tiles/mineral"),
                GetInfo = (game, tile) => "750/800 Resources",
                CollisionBox = Polygon.FromRectangle(new Rectangle(23, 20, 90, 85)),
                Category = TileCategory.Natural
            });
        }
    }
}