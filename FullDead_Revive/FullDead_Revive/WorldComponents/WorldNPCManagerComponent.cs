﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using ViruEngine;
using ViruEngine.ComponentSystem;
using ViruEngine.Entities;
using ViruEngine.Entities.Components;
using ViruEngine.Physics;
using ViruEngine.World;

namespace FullDead_Revive.WorldComponents
{
    internal class WorldNPCManagerComponent : Component<GameLevel>
    {
        public List<GameEntity> NPC = new List<GameEntity>();

        public WorldNPCManagerComponent(ViruGame game, GameLevel parent) : base(game, parent)
        {
        }

        public void AddNPC(GameTime time, GameEntity npc, Vector2 position)
        {
            if (position.X < 0 | position.Y < 0) throw new ArgumentException();

            if (NPC.Contains(npc))
            {
                throw new ArgumentException();
            }

            NPC.Add(npc);

            npc.Position = position;
            if (npc.HasComponent<CollisionComponent>())
                npc.GetComponent<CollisionComponent>().CollisionBox.Offset(new Vector(position));
            npc.SetSpawnedState(time, true);
            Parent.AddEntity(npc);
        }

        public GameEntity CreateNPC(GameTime time, string id, Vector2 position)
        {
            var tile = EntityManager.BuildEntity(id, Game, Parent);
            AddNPC(time, tile, position);

            return tile;
        }

        public GameEntity GetEntityByPosition(Vector2 position)
        {
            foreach (var entity in NPC)
            {
                if (!entity.GetSpawnedState()) continue;

                if (entity.HasComponent<CollisionComponent>())
                {
                    if (Collision.PolygonCollision(entity.GetComponent<CollisionComponent>().CollisionBox,
                        Polygon.FromRectangle(new Rectangle((int) position.X, (int) position.Y, 0, 0)), new Vector(0, 0))
                        .Intersect)
                    {
                        return entity;
                    }
                }
            }

            return null;
        }
    }
}