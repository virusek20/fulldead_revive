﻿using System;
using FullDead_Revive.EntityComponents;
using Microsoft.Xna.Framework;
using ViruEngine;
using ViruEngine.ComponentSystem;
using ViruEngine.Console;
using ViruEngine.Entities;
using ViruEngine.Entities.Components;
using ViruEngine.World;

namespace FullDead_Revive.WorldComponents
{
    internal class WorldTileManagerComponent : Component<GameLevel>
    {
        public GameEntity[,,] Tiles;

        public WorldTileManagerComponent(ViruGame game, GameLevel parent) : base(game, parent)
        {
        }

        public void AllocateTiles(int width, int height)
        {
            Tiles = new GameEntity[width, height, 2];
        }

        public GameEntity GetTileByPosition(Vector2 position, int layer)
        {
            return TryGetEntity(position/128, layer);
        }

        public void UpdateNeighbours(GameEntity updated)
        {
            var position = updated.GetComponent<TileComponent>().TilePosition;

            for (var i = -1; i < 2; i++)
            {
                if (position.X + i < 0 | position.X + i >= Tiles.GetLength(0)) continue;

                for (var j = -1; j < 2; j++)
                {
                    if (position.Y + j < 0 | position.Y + j >= Tiles.GetLength(1)) continue;

                    if (j == 0 & i == 0) continue;

                    for (var k = 0; k < 2; k++)
                    {
                        if (Tiles[(int) (position.X + i), (int) (position.Y + j), k] != null)
                        {
                            if (
                                Tiles[(int) (position.X + i), (int) (position.Y + j), k]
                                    .GetComponent<WorldTileComponent>().TileInfo.OnNeighbourUpdate != null)
                            {
                                Tiles[(int) (position.X + i), (int) (position.Y + j), k]
                                    .GetComponent<WorldTileComponent>()
                                    .TileInfo.OnNeighbourUpdate(Game,
                                        Tiles[(int) (position.X + i), (int) (position.Y + j), k], updated, Parent);
                            }
                        }
                    }
                }
            }
        }

        public GameEntity TryGetEntity(Vector2 position, int layer)
        {
            if (layer > 1 | layer < 0) return null;
            if (position.X < 0 | position.X >= Tiles.GetLength(0)) return null;
            if (position.Y < 0 | position.Y >= Tiles.GetLength(1)) return null;

            return Tiles[(int) position.X, (int) position.Y, layer];
        }

        public void AddTile(GameTime time, GameEntity tile, Vector2 position)
        {
            if (position.X < 0 | position.Y < 0) throw new ArgumentException();

            var tileType = 0;

            if (!tile.GetComponent<WorldTileComponent>().TileInfo.BuildableOver)
            {
                tileType = 1;
            }

            if (Tiles[(int) position.X, (int) position.Y, tileType] != null)
            {
                GameConsole.WriteLine(
                    "Overwriting tile " +
                    Tiles[(int) position.X, (int) position.Y, tileType].GetComponent<WorldTileComponent>().TileInfo.ID +
                    " at [" + position.X + ", " + position.Y + "] with " +
                    tile.GetComponent<WorldTileComponent>().TileInfo.ID, Color.Yellow);

                Tiles[(int) position.X, (int) position.Y, tileType].SetSpawnedState(time, false);
                Tiles[(int) position.X, (int) position.Y, tileType].Deinit(time);
                Parent.Entities.Remove(Tiles[(int) position.X, (int) position.Y, tileType]);
            }

            Tiles[(int) position.X, (int) position.Y, tileType] = tile;

            tile.GetComponent<TileComponent>().TilePosition = position;
            tile.SetSpawnedState(time, true);
            Parent.AddEntity(tile);
        }

        public GameEntity CreateTile(GameTime time, string id, Vector2 position)
        {
            var tile = EntityManager.BuildEntity(id, Game, Parent);
            AddTile(time, tile, position);

            return tile;
        }
    }
}