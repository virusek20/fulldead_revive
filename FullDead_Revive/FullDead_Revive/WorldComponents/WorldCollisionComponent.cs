﻿using FullDead_Revive.EntityComponents;
using Microsoft.Xna.Framework;
using ViruEngine;
using ViruEngine.ComponentSystem;
using ViruEngine.Entities.Components;
using ViruEngine.Physics;
using ViruEngine.World;

namespace FullDead_Revive.WorldComponents
{
    internal class WorldCollisionComponent : Component<GameLevel>
    {
        public WorldCollisionComponent(ViruGame game, GameLevel parent) : base(game, parent)
        {
        }

        public override void Update(GameTime gameTime)
        {
            foreach (var entity in Parent.Entities)
            {
                if (!entity.GetSpawnedState()) continue;

                if (entity.HasComponent<TileComponent>())
                {
                    if (entity.GetComponent<WorldTileComponent>().TileInfo.Walkable) continue;

                    foreach (var npc in Parent.GetComponent<WorldNPCManagerComponent>().NPC)
                    {
                        npc.GetComponent<CollisionComponent>()
                            .CollideWorld(gameTime, npc, entity,
                                Collision.PolygonCollision(npc.GetComponent<CollisionComponent>().CollisionBox,
                                    entity.GetComponent<TileComponent>().CollisionBox,
                                    new Vector(npc.GetComponent<VelocityComponent>().Velocity)));
                    }
                }
                else if (entity.HasComponent<InfectedComponent>())
                {
                    foreach (var npc in Parent.GetComponent<WorldNPCManagerComponent>().NPC)
                    {
                        if (!npc.GetSpawnedState()) continue;
                        if (npc == entity) continue;
                        if (!npc.HasComponent<PlayerComponent>()) continue;

                        npc.GetComponent<CollisionComponent>()
                            .CollideEntity(gameTime, entity, npc,
                                Collision.PolygonCollision(entity.GetComponent<CollisionComponent>().CollisionBox,
                                    npc.GetComponent<CollisionComponent>().CollisionBox,
                                    new Vector(entity.GetComponent<VelocityComponent>().Velocity)), false);
                    }
                }
            }
        }
    }
}